﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

[Serializable]
public class Monster
{    
    public string Name;
    public int Health;

    [XmlArray("Skills"), XmlArrayItem("Skill")]
    public List<int> Skills;

    public void SaveToXml(string path)
    {
        UnityEngine.Debug.Log("Saving to XML on the path: " + path);

        var serializer = new XmlSerializer(typeof(Monster));
        using (var stream = new FileStream(path, FileMode.Create))
        {
            serializer.Serialize(stream, this);
        }
    }

    //public void Save(string path)
    //{
    //    var serializer = new XmlSerializer(typeof(MonsterContainer));
    //    using (var stream = new FileStream(path, FileMode.Create))
    //    {
    //        serializer.Serialize(stream, this);
    //    }
    //}
}