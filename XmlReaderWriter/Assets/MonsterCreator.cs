﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;

public class MonsterCreator : MonoBehaviour
{
    public TextAsset LoadFromAsset;
    
    public MonsterCollection PersistentCollection = new MonsterCollection();

    //Collection we're creating at runtime.
    public MonsterCollection Collection;

    [ContextMenu("Teste Persistent Data")]
    public void TestePersistentData()
    {
        List<Monster> monsters = new List<Monster>();

        for (int i = 0; i < 5; i++)
        {
            Monster newMonster = new Monster();
            newMonster.Name = "Monster" + Random.Range(0, 100);
            newMonster.Health = Random.Range(0, 200);

            newMonster.Skills = new List<int>();

            for (int j = 0; j < 3; j++)
            {
                newMonster.Skills.Add(UnityEngine.Random.Range(0, 10));
            }

            monsters.Add(newMonster);
        }

        if (PersistentCollection.Monsters == null)
            PersistentCollection.Monsters = new List<Monster>();
        
        PersistentCollection.Monsters.AddRange(monsters);

        PersistentCollection.SaveToXml(Path.Combine(Application.dataPath, "persistentCollection.xml"));
    }

    [ContextMenu("Teste Save")]
    public void CreateMonsterXml()
    {
        Monster newMonster1 = new Monster();
        newMonster1.Name = "Monster 1";
        newMonster1.Health = 10;
        newMonster1.Skills = new List<int>() {1, 2, 3};
        
        Monster newMonster2 = new Monster();
        newMonster2.Name = "Monster 2";
        newMonster2.Health = 5;
        newMonster2.Skills = new List<int>() {4, 5, 6};

        MonsterCollection collection = new MonsterCollection();
        collection.Monsters.Add(newMonster1);
        collection.Monsters.Add(newMonster2);

        collection.SaveToXml(Path.Combine(Application.dataPath, "monsters.xml"));

        //newMonster.SaveToXml(Path.Combine(Application.dataPath, "monster.xml"));
    }

    [ContextMenu("Test Load")]
    public void LoadFromXml()
    {
        Collection = new MonsterCollection();

        UnityEngine.Debug.Log("Read Asset: " + LoadFromAsset.text);

        var serializer = new XmlSerializer(typeof(MonsterCollection));
        Collection = serializer.Deserialize(new StringReader(LoadFromAsset.text)) as MonsterCollection;
    }
}