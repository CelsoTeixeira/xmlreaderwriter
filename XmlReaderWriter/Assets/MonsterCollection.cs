﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

[XmlRoot("MonsterCollection")]
[Serializable]
public class MonsterCollection
{
    //[XmlArray("Monsters"), XmlArrayItem("Monster")]
    public List<Monster> Monsters = new List<Monster>();

    public void SaveToXml(string path)
    {
        UnityEngine.Debug.Log("Saving to XML on the path: " + path);

        var serializer = new XmlSerializer(typeof(MonsterCollection));
        using (var stream = new FileStream(path, FileMode.Create))
        {
            serializer.Serialize(stream, this);
        }
    }
}