﻿using System.IO;
using System.Xml.Serialization;
using UnityEngine;


[CreateAssetMenu(fileName = "MonsterDatabase", menuName = "Create new Monster DB")]
public class MonsterDatabase : ScriptableObject
{
    public string DatabaseName = "MonstersDatabase";

    //XML we want to read and transform on our Collection.
    public TextAsset LoadFromAsset;

    //Actual Collection
    public MonsterCollection Collection;

    [ContextMenu("Test Load")]
    public void LoadFromXml()
    {
        Collection = new MonsterCollection();

        UnityEngine.Debug.Log("Read Asset: " + LoadFromAsset.text);

        var serializer = new XmlSerializer(typeof(MonsterCollection));
        Collection = serializer.Deserialize(new StringReader(LoadFromAsset.text)) as MonsterCollection;
    }

    public void SaveToXml()
    {
        if (Collection == null)
        {
            UnityEngine.Debug.Log("There's nothing to be saved inside the database!");
            return;
        }

        UnityEngine.Debug.Log("Saving to XML!");

        var serializer = new XmlSerializer(typeof(MonsterCollection));
        using (var stream = new FileStream(Path.Combine(Application.dataPath, DatabaseName), FileMode.Create))
        {
            serializer.Serialize(stream, Collection);
        }
    }
}